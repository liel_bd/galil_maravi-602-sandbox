from scapy.all import wrpcap, sniff
import os, sys, logging

logging.basicConfig(level=logging.DEBUG)


def save_packets(pkts):
	#  the path in the vm
	wrpcap(r"C:\Users\IEUser\Desktop\Sniffer\packets.pcap", pkts)


def pkt_callback(pkt):
	logging.info(pkt.summary())


def on_sniff():
	pkts = sniff(prn=pkt_callback, store=150, count=100)
	save_packets(pkts)


def main():
	on_sniff()


if __name__ == "__main__":
	main()
