from scapy.all import IP, Raw, rdpcap, TCP, UDP, Ether, ICMP
import logging, requests
import socket


def load_packets(file_path):
    """
    A function that load the pcap file
    :param file_path: the file path to the pcap file
    :return: the packets in the pcap file
    """
    logging.basicConfig(level=logging.DEBUG, filename='NetLog.log')
    packets = rdpcap(file_path)
    return packets


def find_my_ip():
    """
    A function that find the ip address of the current computer
    :return: ip address of the current computer
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    my_ip = s.getsockname()[0]
    s.close()
    return my_ip


def find_ips(pckts):
    """
    A function that find and extract all the ips that the file go to
    :param pckts: the packets
    :return: none
    """
    address = []
    my_ip = find_my_ip()
    # get all the ip addresses
    for packet in pckts:
        # suspicious.json
        if IP in packet:
            if Raw in packet:
                logging.info(packet[IP].src + " ---- " + packet[IP].dst + " The data = " + str(packet[Raw]))
            else:
                logging.info(packet[IP].src + " ---- " + packet[IP].dst + " Without data")
            if packet[IP].src == my_ip and packet[IP].dst not in address:
                address.append(packet[IP].dst)
            elif packet[IP].src not in address:
                address.append(packet[IP].src)

    malicious_ip(address)


def malicious_ip(ip_address):
    """
    A function that check if the file access to ip addresses that malicious
    :param ip_address: the list of the all the ip addresses
    :return: none
    """
    block = [
        "http://opendbl.net/lists/etknown.list",
        "http://opendbl.net/lists/tor-exit.list",
        "http://opendbl.net/lists/bruteforce.list",
        "http://opendbl.net/lists/blocklistde-all.list",
        "http://opendbl.net/lists/talos.list",
        "http://opendbl.net/lists/dshield.list",
        "http://opendbl.net/lists/malwaredomain.list",
        "http://opendbl.net/lists/sslblock.list",
        "http://opendbl.net/lists/zeustracker.list",
    ]

    malicious_str = ""

    for site in block:
        r = requests.get(site)
        malicious_str += r.text  # add all the malicious ips to a string

    # check for each ip
    for ip in ip_address:
        if ip in malicious_str:
            print(ip + " is malicious")


def handler(file_path):
    """
    A function that handle all the functions
    :param file_path: the pcap file
    :return: none
    """
    packets = load_packets(file_path)

    TCP_count = 0
    UDP_count = 0
    for pkt in packets:
        if TCP in pkt:
            TCP_count += 1

        if UDP in pkt:
            UDP_count += 1

    logging.debug(TCP_count)
    logging.debug(UDP_count)

    find_ips(packets)