import os
import subprocess
import sys
import my_parser

# get the current dir
PROJECT_PATH = os.getcwd()
DIR_PATH = '\\'.join(PROJECT_PATH.split('\\')[0:-2]) + r'\DynamicScan'

# the files in the dir
DLL_LOG_PATH = DIR_PATH + r"\log.txt"
SNIFF_LOG_PATH = DIR_PATH + r"\NetLog.log"
PACKETS_PATH = DIR_PATH + r"\packets.pcap"
VM_PATH = DIR_PATH + r"\vm.exe"


def run_scans(file_path, vmx_path):
    """
    A function that run the vm and print the results of the dynamic scans
    :param file_path: the file path
    :return: none
    """
    # delete the previous result's files
    if os.path.exists(DLL_LOG_PATH):
        os.remove(DLL_LOG_PATH)  # one file at a time
    if os.path.exists(PACKETS_PATH):
        os.remove(PACKETS_PATH)  # one file at a time
    if os.path.exists(SNIFF_LOG_PATH):
        os.remove(SNIFF_LOG_PATH)  # one file at a time

    subprocess.run([VM_PATH, file_path, vmx_path],
                   stdout=subprocess.DEVNULL,
                   stderr=subprocess.STDOUT)

    try:
        # print the result of dll hooking
        log = open(DLL_LOG_PATH, 'r').readlines()
        log_text = "\n".join(sorted(set(list(filter(lambda line: "FILE_READ" not in line, log)))))
        print(log_text)
    except:
        print('Not Found')

    print('&&&')

    # run and print the result of the sniffer
    my_parser.handler(PACKETS_PATH)


def main(file_path, vmx_path):
    run_scans(file_path, vmx_path)


if __name__ == "__main__":
    file = sys.argv[1]
    vmx_path = sys.argv[2]
    main(file, vmx_path)