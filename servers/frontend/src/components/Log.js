import { Card, Row, Col } from "react-bootstrap";
import Modal from "./UI/Modal";
import "./DownloadButton";
import DownloadButton from "./DownloadButton";
import React from "react";
const Log = (props) => {
  return (
    <Modal>
      <Card className="text-light bg-dark">
        <Card.Body>
          <Card.Title className="mb-2" style={{ textAlign: "center" }}>
            LOG
          </Card.Title>
          <Card.Text>
            {/* virustotal */}
            {props.virustotal && props.virustotal == 'Not Found' && (
              <div>
                <b>VirusTotal</b>
                &emsp;
                {
                  <div>
                    <span style={{ color: "#31AA52" }}>
                      <b>
                        &emsp;
                        {props.virustotal}
                      </b>
                    </span>
                  </div>
                }
                <br />
                <br />
              </div>
            )}

            
            {props.virustotal && props.virustotal != 'Not Found' && (
              <div>
                <b>VirusTotal</b>
                &emsp;
                {
                  <div>
                    <span style={{ color: "#EB4132" }}>
                      <b>
                        &emsp;
                        {props.virustotal.substring(
                          0,
                          props.virustotal.indexOf(" ")
                        )}
                      </b>
                    </span>
                    {props.virustotal.substring(
                      props.virustotal.indexOf(" "),
                      props.virustotal.lastIndexOf(" ") + 1
                    )}
                    <span style={{ color: "#31AA52" }}>
                      <b>
                        {props.virustotal.substring(
                          props.virustotal.lastIndexOf(" ") + 1
                        )}
                      </b>
                    </span>
                  </div>
                }
                <br />
                <br />
              </div>
            )}

            {/* yara */}
            {props.yara && (
              <div>
                <b>Yara</b>
              </div>
            )}
            {props.yara &&
              props.yara.map((rule) => {
                return (
                  <p>
                    &emsp;
                    <span style={{ color: "#EB4132" }}>
                      <b>{rule}</b>
                    </span>{" "}
                  </p>
                );
              })}

            {/* dll hooking */}
            {props.dll && (
              <div>
                <br />
                <br />
                <b>Dll Hooking</b>
              </div>
            )}
            {props.dll &&
              props.dll.map((syscall) => {
                return (
                  <p>
                    &emsp;
                    <span style={{ color: "#EB4132" }}>
                      <b>{syscall}</b>
                    </span>
                  </p>
                );
              })}

            {/* sniffer */}
            {props.sniffer &&
              <div>
                <br />
                <br />
                <Row xs="auto">
                  <Col><b>Sniffer</b></Col>
                  <Col className="mb-3"><DownloadButton /></Col>
                </Row>
                
              </div>
            }
            {props.sniffer &&
              props.sniffer.map((ip) => {
                return (
                  <p>
                    &emsp;
                    <span style={{ color: "white" }}>
                      <b>{ip}</b>
                    </span>
                  </p>
                );
              })}
          </Card.Text>
        </Card.Body>
      </Card>
    </Modal>
  );
};

export default Log;
