import Axios from "axios";
import FileDownload from 'js-file-download'
import React from "react";
import {Button} from 'react-bootstrap'

function DownloadButton() {
    const download=(e)=>{
        e.preventDefault()
        Axios({
            url: 'http://localhost:3001',
            method: 'GET',
            responseType: 'blob',
        }).then((res) => {
            console.log(res)
            FileDownload(res.data, 'NetLog.log')
        })

    }

    return (
        <div>
            <Button variant="outline-info" onClick={e => download(e)}>Download NetLog</Button>
        </div>
    )
}

export default DownloadButton;