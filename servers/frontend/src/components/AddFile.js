import Modal from "./UI/Modal";
import { Form, Button } from "react-bootstrap";
import { useState } from "react";
import { SpinnerDotted } from "spinners-react";
import React from "react";

const AddFile = (props) => {
  const [isLoading, setIsLoading] = useState(false);
  const [file, setFile] = useState("");
  const [vmxPath, setVmxPath] = useState("");

  const submitHandler = async (event) => {
    setIsLoading(true);
    event.preventDefault();
    await props.onSubmit(file, vmxPath);
    setIsLoading(false);
  };

  const pathInputChangeHandler = (event) => {
    setVmxPath(event.target.value);
  };

  const onUploadFile = (event) => {
    setFile(event.target.files[0]);
  };

  return (
    <Modal>
      <Form onSubmit={submitHandler} className="p-4 text-light bg-dark">
        <Form.Group className="mb-5">
          <Form.Label className="mb-3">Add virtual machine path (vmx file)</Form.Label>
          <Form.Control
            onChange={pathInputChangeHandler}
            type="text"
            placeholder="Enter vmx path located in your computer"
            required
          />
        </Form.Group>

        <Form.Group>
          <Form.Label className="mb-3">Add file for scanning</Form.Label>
          <Form.Control onChange={onUploadFile} type="file" required />
        </Form.Group>

        <div className="text-center mt-5">
          {isLoading ? (
            <SpinnerDotted size={38} thickness={140} />
          ) : (
            <Button variant="success" type="submit">
              Submit
            </Button>
          )}
        </div>
      </Form>
    </Modal>
  );
};

export default AddFile;
