import AddFile from "./AddFile";
import Log from "./Log";
import axios from "axios";
import { useState } from "react";
import React from "react";
const Handler = () => {
  const [resutls, setResults] = useState({});

  const submitHandler = async (file, path) => {
    setResults({});
    const data = new FormData();
    data.append("file", file);
    data.append("path", path);

    const response = await axios.post("http://localhost:3001/upload", data, {});
    const logData = await response.data;
    setResults(logData);
  };

  return (
    <>
      <AddFile onSubmit={submitHandler} />
      <Log
        virustotal={resutls.virustotal}
        yara={resutls.yara}
        dll={resutls.dll}
        sniffer={resutls.sniffer}
      />
    </>
  );
};

export default Handler;
