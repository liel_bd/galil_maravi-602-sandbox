const mongoose = require('mongoose')
const Schema = mongoose.Schema

const FileSchema = new Schema({
    signature: {
        type: String,
        unique: false
    },
    date: String,
})

module.exports = mongoose.model('File', FileSchema)