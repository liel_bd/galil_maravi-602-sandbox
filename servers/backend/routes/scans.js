const express = require("express");
const router = express.Router();
const File = require("../models/scan");
const formidable = require("formidable");
const scansHandler = require("../handlers/scansHandler");
const fs = require("fs");

router.post("/upload", async (req, res) => {
  const form = new formidable.IncomingForm();
  form.parse(req, async function (err, fields, files) {
    const file_path = files.file.filepath;
    const vmx_path = fields.path;

    const signature = scansHandler.getFileSignature(file_path);
    const date = scansHandler.getDate();

    //check if the file in the db
    File.findOne({ signature: signature }, async function(err, isFound) {
      if(isFound != null) // if found
      {
        const bufferData = fs.readFileSync(`filesChecked/${signature}.json`);
        const stData = bufferData.toString();
        const data = JSON.parse(stData);
        res.json(data);
      } else {
        // save in the db
        const file = new File({ signature, date });
        await file.save();
  
        //run all the scans
        res.json(await scansHandler.scanFile(file_path, vmx_path, signature));
      }
    });

  });
});

module.exports = router;
