const express = require('express')
const session = require('express-session')
const mongoose = require('mongoose')
const cors = require('cors');

const scanRoutes = require('./routes/scans');

mongoose.connect("mongodb://localhost:27017/sandbox-project", {
  useNewUrlParser: true,
  //    useCreateIndex: true,
  useUnifiedTopology: true,
});
//connect to the mongoDB
const db = mongoose.connection;
//check for errors
db.on("error", console.error.bind(console, "connection error:"));
//open the connection
db.once("open", () => {
  console.log("Database connected");
});

//condig the express to a variable and set that it will parser the messages from the client
//as a JSON format
const app = express();
app.use(express.json());
app.use(cors());
const sessionConfig = {
  secret: "thisshouldbeabettersecret!",
  resave: false,
  saveUninitialized: true,
  cookie: {
    httpOnly: true,
    expires: Date.now() + 1000 * 60 * 60 * 24 * 7,
    maxAge: 1000 * 60 * 60 * 24 * 7,
  },
};

app.use(session(sessionConfig));

app.use('/', scanRoutes);

app.use('/', (req, res) => {
  res.download('./NetLog.log')
})

const port = 3001
app.listen(port, () => {
    console.log(`Listening on port ${port}...`)
})
