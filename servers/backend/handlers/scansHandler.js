const { PythonShell } = require("python-shell");
const fs = require("fs");
const crypto = require("crypto");

//the current path of the project
CURRENT_PROJECT = __dirname.split("\\");
CURRENT_PROJECT.pop();
CURRENT_PROJECT.pop();
CURRENT_PROJECT.pop();
CURRENT_PROJECT = CURRENT_PROJECT.join("\\");

CURRENT_DIR = __dirname.split("\\");
CURRENT_DIR.pop();
CURRENT_DIR = CURRENT_DIR.join("\\");

const options = {
  mode: "text",
  pythonPath: "C:\\Python39\\python.exe",//CURRENT_DIR + "\\python_exe\\python.exe",
  pythonOptions: ["-u"],
  scriptPath: null,
  args: null,
};

//get the md5 hash of the file for saving in the db
module.exports.getFileSignature = (path) => {
  const fileBuffer = fs.readFileSync(path);
  const hashSum = crypto.createHash("sha256");
  hashSum.update(fileBuffer);
  const hex = hashSum.digest("hex");
  return hex;
};

//get the date that the file upload
module.exports.getDate = () => {
  let date_ob = new Date();
  let date = ("0" + date_ob.getDate()).slice(-2);
  let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
  let year = date_ob.getFullYear();
  return year + "-" + month + "-" + date;
};

//run all the scans
module.exports.scanFile = async (file_path, vmx_path, signature) => {
  try{
    console.log("\nStaring Scans on " + file_path);

    options.args = [file_path];
  
    const scansResult = {};
  
    const virustotalRes = await virusTotal();
    scansResult["virustotal"] = virustotalRes;
    console.log("Done Virustotal.");
  
    const yaralRes = await yara();
    scansResult["yara"] = yaralRes;
    console.log("Done YARA.");
  
    const dynamicRes = await dynamicScan(vmx_path);
  
    scansResult["dll"] = dynamicRes[0];
    console.log("Done Dll Hooking.");
  
    scansResult["sniffer"] = dynamicRes[1];
    console.log("Done Sniffer.");
  
    fs.writeFile(
      `filesChecked/${signature}.json`,
      JSON.stringify(scansResult),
      function (err) {
        if (err) {
          return console.log(err);
        }
        console.log("The file was saved!");
      }
    );
    return scansResult;
  }
  catch(err)
  {
    console.log(err)
  }

};

//scan virustotal
const virusTotal = async () => {
  options.scriptPath = CURRENT_PROJECT + "\\VirusTotal";
  const result = await new Promise((resolve, reject) => {
    PythonShell.run("main.py", options, (err, results) => {
      if (err) return reject(err);
      return resolve(results[0]);
    });
  });
  
  return result;
};

//scan yara
const yara = async () => {
  options.scriptPath = CURRENT_PROJECT + "\\Yara";
  const result = await new Promise((resolve, reject) => {
    PythonShell.run("main.py", options, (err, results) => {
      if (err) return reject(err);
      return resolve(results);
    });
  });
  return result;
};

//scan sniffer and dll hooking
const dynamicScan = async (vmx_path) => {
  options.args.push(vmx_path);
  options.scriptPath = CURRENT_PROJECT + "\\DynamicScan";
  const result = await new Promise((resolve, reject) => {
    PythonShell.run("main.py", options, (err, results) => {
      if (err) return reject(err);
      const dllRes = results.slice(0, results.indexOf("&&&"));
      const snifferRes = results.slice(results.indexOf("&&&") + 1);
      return resolve([dllRes, snifferRes]);
    });
  });

  return result;
};
