import requests
import sys


def get_file_resource(file_path):
    """
    A function that scan the file in virustotal
    :param file_path: the file path
    :return: the result of the scanning (resource)
    """

    params = {'apikey': '45a150f762685d44831bf9fde6bc26ca8be1f2708439c7ecfbb5fea10d2aadbd'}
    files = {'file': (file_path, open(file_path, 'rb'))}

    # scanning the file
    response = requests.post('https://www.virustotal.com/vtapi/v2/file/scan', files=files, params=params)
    json_response = response.json()
    resource = json_response['resource']

    return resource


def file_report(resource):
    """
    A function that get a report for the file through the resource
    :param resource: the resource for the file
    :return: the amount of antivirus that reported the file was infected, the total antivirus
    """

    params = {'apikey': '45a150f762685d44831bf9fde6bc26ca8be1f2708439c7ecfbb5fea10d2aadbd', 'resource': resource}
    headers = {
    "Accept-Encoding": "gzip, deflate",
    "User-Agent" : "gzip,  My Python requests library example client or username"
    }

    # send a request for the report
    response = requests.get('https://www.virustotal.com/vtapi/v2/file/report',
    params=params, headers=headers)

    json_response = response.json() # convert to json
    
    positives = json_response["positives"]  # get the scanning result
    total = json_response["total"]  # get the total scans

    return positives, total


def main(file_path):
    try:
        resource = get_file_resource(file_path)
        positives, total = file_report(resource)
        print(str(positives) + " security vendors from total of " + str(total))
    except:
        print('Not Found')


if __name__ == "__main__":
    file = sys.argv[1]
    main(file)