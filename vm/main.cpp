#include <iostream>
#include <windows.h>
#include <string>
#include "vmware_virtual_machine.h"



std::string vmx_path = "";
const std::string username = "IEUser";
const std::string password = "Passw0rd!";

/// <summary>
///		A function that delete files left in previous scans in case of need
/// </summary>
void deleteInitalFiles(sandbox::vmware_virtual_machine& vm) {
	try //try to delete the malware from sniffer
	{
		vm.run_cmd("del C:\\Users\\IEUser\\Desktop\\Sniffer\\malware.exe");
	}
	catch (const std::exception& e) {}

	try //try to delete the pcap
	{
		vm.run_cmd("del C:\\Users\\IEUser\\Desktop\\Sniffer\\packets.pcap");
	}
	catch (const std::exception& e) {}

	try //try to delete the malware from DllHooking
	{
		vm.run_cmd("del C:\\Users\\IEUser\\Desktop\\DllHooking\\malware.exe");
	}
	catch (const std::exception& e) {}

	try //try to delete the result
	{
		vm.run_cmd("del C:\\Users\\IEUser\\Desktop\\DllHooking\\result.txt");
	}
	catch (const std::exception& e) {}
}


/// <summary>
///		A function that get the current path
/// </summary>
/// 
/// <returns>
///		the current path
/// </returns>
std::string currentPath() {
	TCHAR buffer[MAX_PATH] = { 0 };
	GetModuleFileName(NULL, buffer, MAX_PATH);
	std::wstring::size_type pos = std::wstring(buffer).find_last_of(L"\\/");
	std::wstring path = std::wstring(buffer).substr(0, pos);
	std::string strPath(path.begin(), path.end());
	return strPath.substr(0, strPath.find_last_of('vm') - 2);
}

/// <summary>
///		A function that run the dll hooking scan
/// </summary>
/// 
/// <param name="malwarePath">
///		the path of the file
/// </param>
void run_dll_hooking(std::string malwarePath, sandbox::vmware_virtual_machine& vm)
{
	std::string currPath = currentPath();
	vm.transfer_file_to_guest(malwarePath, "C:\\Users\\IEUser\\Desktop\\DllHooking\\malware.exe"); //transfer the malware to the vm

	//check the malware with the malware installed in the vm
	vm.run_exe("C:\\Users\\IEUser\\Desktop\\DllHooking\\Injector.exe", "C:\\Users\\IEUser\\Desktop\\DllHooking\\malware.exe");

	//get the result file
	vm.transfer_file_to_host("C:\\Users\\IEUser\\Desktop\\DllHooking\\result.txt",
		currPath.substr(0, currPath.find_last_of('\\')) + "\\DynamicScan\\log.txt");

	//delete the result and the malware
	vm.run_cmd("del C:\\Users\\IEUser\\Desktop\\DllHooking\\result.txt");
	vm.run_cmd("del C:\\Users\\IEUser\\Desktop\\DllHooking\\malware.exe");
}

/// <summary>
///		A function that run the sniffer scan
/// </summary>
/// 
/// <param name="malwarePath">
///		the path of the file
/// </param>
void run_sniffer(std::string malwarePath, sandbox::vmware_virtual_machine& vm)
{
	std::string currPath = currentPath();
	vm.transfer_file_to_guest(malwarePath, "C:\\Users\\IEUser\\Desktop\\Sniffer\\malware.exe"); //transfer the malware to the vm

	vm.run_cmd("python C:\\Users\\IEUser\\Desktop\\Sniffer\\handler.py C:\\Users\\IEUser\\Desktop\\Sniffer\\malware.exe");

	//get the result file
	vm.transfer_file_to_host("C:\\Users\\IEUser\\Desktop\\Sniffer\\packets.pcap",
		currPath.substr(0, currPath.find_last_of('\\')) + "\\DynamicScan\\packets.pcap");


	//delete the pcap and the malware
	vm.run_cmd("del C:\\Users\\IEUser\\Desktop\\Sniffer\\packets.pcap");
	vm.run_cmd("del C:\\Users\\IEUser\\Desktop\\Sniffer\\malware.exe");
}

int main(int argc, char** argv) {	
	try {
		vmx_path = argv[2];

		sandbox::vmware_virtual_machine vm(vmx_path);

		vm.login(username, password); //login to the vm

		deleteInitalFiles(vm);

		//VixHandle handle = vm.create_snapshot("snapshot", true); //save a snapshot

		const std::string malwarePath = argv[1];

		run_dll_hooking(malwarePath, vm);
		run_sniffer(malwarePath, vm);

		//vm.revert_to_snapshot(handle); //revert the snapshot
		//vm.remove_snapshot(handle); // delete the snapshot

		vm.shutdown();
		

	} catch ( const sandbox::virtual_machine_exception &e ) {
		std::cerr << e.what() << std::endl;

	} catch ( const std::exception &e ) {
		std::cerr << e.what() << std::endl;
	} catch ( ... ) {
		std::cerr << "Occured unknown error" << std::endl;
	}
}