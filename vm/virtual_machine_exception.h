#ifndef VIRTUAL_MACHINE_EXCEPTION_H
#define VIRTUAL_MACHINE_EXCEPTION_H

#include <exception>
#include <string>
#include <string_view>

namespace sandbox {
	class virtual_machine_exception : public std::exception {
	public:
		/**
		 * @brief Constructs an exception
		 * @param message Exception message
		 */
		virtual_machine_exception( std::string_view message ) :
			message_( message ) {}

		/**
		 * @brief Return exception message
		 * @return Exception message
		 */
		virtual const char *what() const {
			return message_.c_str();
		}

	protected:
		std::string message_; /// exception message
	};
} // namespace sandbox

#endif // VIRTUAL_MACHINE_EXCEPTION_H