#ifndef VMWARE_VIRTUAL_MACHINE_H
#define VMWARE_VIRTUAL_MACHINE_H

#include "virtual_machine.h"
#include "virtual_machine_exception.h"
#include "C:\\Program Files (x86)\\VMware\\VMware VIX\\vix.h"

namespace sandbox {
	class vmware_virtual_machine : public virtual_machine {
		std::filesystem::path vmx_path_; /// vmx config file path

		/// host and vm handles
		VixHandle host_ = VIX_INVALID_HANDLE;
		VixHandle vm_ = VIX_INVALID_HANDLE;

		bool is_logged_in_ = false; /// is logged into user 
		
	public:
		/**
		 * @brief Constructs object
		 * @param vmx_path Path to vmx-config file
		 */
		explicit vmware_virtual_machine( const std::filesystem::path &vmx_path );
		/**
		 * @brief Clears memory
		 */
		virtual ~vmware_virtual_machine();

		/**
		 * @brief Return is there connection between api and vmware
		 * @return Is there connection between api and vmware
		 */
		inline virtual bool is_connected() const {
			return host_ != VIX_INVALID_HANDLE;
		}
		/**
		 * @brief Returns is vm booted
		 * @return Is vm booted
		 */
		inline virtual bool is_booted() const {
			return vm_ != VIX_INVALID_HANDLE;
		}
		/**
		 * @brief Returns is there logged user
		 * @return Is there logged user
		 */
		inline virtual bool is_logged_in() const {
			return is_logged_in_;
		}

		/**
		 * @brief Connects to the VMWare
		 */
		virtual void connect();
		/**
		 * @brief Disconnects from the VMWare
		 */
		virtual void disconnect();

		/**
		 * @brief Boots the vm image
		 */
		virtual void boot();
		/**
		 * @brief Shutdowns the vm image
		 */
		virtual void shutdown();

		/**
		 * @brief Signs Into account
		 * @param username Account's username
		 * @param password Account's password
		 */
		virtual void login( std::string_view username, std::string_view password );
		/**
		 * @brief Signs Out from the account
		 */
		virtual void logout();

		/**
		 * @brief Transfers files from host system to guest system
		 * @param src File path on host system
		 * @param dst File path on guest system
		 */
		virtual void transfer_file_to_guest( const std::filesystem::path &src, const std::filesystem::path &dst ) const;
		/**
		 * @brief Transfers files from guest system to host system
		 * @param src File path on guest system
		 * @param dst File path on host system
		 */
		virtual void transfer_file_to_host( const std::filesystem::path &src, const std::filesystem::path &dst ) const;
	
		/**
		 * @brief Runs command on guest system
		 * @param command Command to perform
		 */
		virtual void run_cmd( std::string_view command ) const;
		/**
		 * @brief Runs executable on guest system
		 * @param exe_path Executable path
		 * @param params Run parameters
		 */
		virtual void run_exe( const std::filesystem::path &exe_path, std::string_view params = "" ) const;

		/**
		 * @brief create a snapshot for the vm
		 * @param name The name of the snapshot
		 * @param include_memory Save the memory of the vm
		 */
		virtual VixHandle create_snapshot(std::string_view name, bool include_memory) const;

		/**
		 * @brief remove a snapshot for the vm
		 * @param snapshot The snapshot to remove
		 */
		virtual void remove_snapshot(VixHandle& snapshot) const;

		/**
		 * @brief revert a snapshot for the vm
		 * @param snapshot The snapshot to revert
		 */
		virtual void revert_to_snapshot(VixHandle snapshot) const;
	};
} // namespace sandbox

#endif // VMWARE_VIRTUAL_MACHINE_H