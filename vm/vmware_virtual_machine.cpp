#include "vmware_virtual_machine.h"
#include <thread>

sandbox::vmware_virtual_machine::vmware_virtual_machine( const std::filesystem::path &vmx_path ) :
	vmx_path_( vmx_path ) {}

sandbox::vmware_virtual_machine::~vmware_virtual_machine() {
	disconnect();
}

void sandbox::vmware_virtual_machine::connect() {
	if ( is_connected())
		return;

	// create connect job and wait until it ends
	auto job = VixHost_Connect( VIX_API_VERSION, VIX_SERVICEPROVIDER_VMWARE_WORKSTATION, nullptr, 0, nullptr, nullptr, 0, VIX_INVALID_HANDLE, nullptr, nullptr );
	auto error = VixJob_Wait( job, VIX_PROPERTY_JOB_RESULT_HANDLE, &host_, VIX_PROPERTY_NONE );
	Vix_ReleaseHandle( job ); // release job handler
	if ( VIX_FAILED( error ) ) {
		VixHost_Disconnect( host_ );
		throw sandbox::virtual_machine_exception( Vix_GetErrorText( error, nullptr ) );
	}
}

void sandbox::vmware_virtual_machine::disconnect() {
	if ( is_logged_in() )
		logout();
	
	if ( is_booted() )
		shutdown();

	VixHost_Disconnect( host_ );
}

void sandbox::vmware_virtual_machine::boot() {
	// connect to vmware if its not connected
	if ( !is_connected() )
		connect();
	
	// release vm handler if its exists
	if ( vm_ != VIX_INVALID_HANDLE )
		shutdown();

	// open vm
	auto job = VixVM_Open( host_, vmx_path_.string().c_str(), nullptr, nullptr );
	auto error = VixJob_Wait( job, VIX_PROPERTY_JOB_RESULT_HANDLE, &vm_, VIX_PROPERTY_NONE );
	Vix_ReleaseHandle( job );
	if ( VIX_FAILED( error ) ) {
		Vix_ReleaseHandle( vm_ );
		throw sandbox::virtual_machine_exception( Vix_GetErrorText( error, nullptr ) );
	}

	// power on
	job = VixVM_PowerOn( vm_, VIX_VMPOWEROP_LAUNCH_GUI, VIX_INVALID_HANDLE, nullptr, nullptr );
	error = VixJob_Wait( job, VIX_PROPERTY_NONE );
	Vix_ReleaseHandle( job );
	if ( VIX_FAILED( error ) ) {
		Vix_ReleaseHandle( vm_ );
		throw sandbox::virtual_machine_exception( Vix_GetErrorText( error, nullptr ) );
	}

	// wait for full windows loaded
	job = VixVM_WaitForToolsInGuest( vm_, /* 5 minutes */ 300, nullptr, nullptr );
	error = VixJob_Wait( job, VIX_PROPERTY_NONE );
	Vix_ReleaseHandle( job );
	if ( VIX_FAILED( error ) ) {
		shutdown();
		throw sandbox::virtual_machine_exception( Vix_GetErrorText( error, nullptr ) );
	}
}

void sandbox::vmware_virtual_machine::shutdown() {
	if ( is_logged_in() )
		logout();

	VixHandle job;
	VixError error;
	do {
		job = VixVM_PowerOff( vm_, VIX_VMPOWEROP_FROM_GUEST, nullptr, nullptr );
		error = VixJob_Wait( job, VIX_PROPERTY_NONE );
		Vix_ReleaseHandle( job );
	} while ( VIX_FAILED( error ) );

	Vix_ReleaseHandle( vm_ );
}

void sandbox::vmware_virtual_machine::login( std::string_view username, std::string_view password ) {
	if ( !is_booted() )
		boot();

	auto job = VixVM_LoginInGuest( vm_, username.data(), password.data(), 0, nullptr, nullptr );
	auto error = VixJob_Wait( job, VIX_PROPERTY_NONE );
	Vix_ReleaseHandle( job );
	if ( VIX_FAILED( error ) ) {
		Vix_ReleaseHandle( vm_ );
		throw sandbox::virtual_machine_exception( Vix_GetErrorText( error, nullptr ) );
	}

	is_logged_in_ = true;
}

void sandbox::vmware_virtual_machine::logout() {
	auto job = VixVM_LogoutFromGuest( vm_, nullptr, nullptr );
	auto error = VixJob_Wait( job, VIX_PROPERTY_NONE );
	Vix_ReleaseHandle( job );

	is_logged_in_ = false;
}

void sandbox::vmware_virtual_machine::transfer_file_to_guest( const std::filesystem::path &src, const std::filesystem::path &dst ) const {
	if ( !is_logged_in() )
		throw sandbox::virtual_machine_exception( "There is not logged user" );

	auto job = VixVM_CopyFileFromHostToGuest( vm_, src.string().c_str(), dst.string().c_str(), 0, VIX_INVALID_HANDLE, nullptr, nullptr );
	auto error = VixJob_Wait( job, VIX_PROPERTY_NONE );
	Vix_ReleaseHandle( job );
	if ( VIX_FAILED( error ) )
		throw sandbox::virtual_machine_exception( Vix_GetErrorText( error, nullptr ) );
}

void sandbox::vmware_virtual_machine::transfer_file_to_host( const std::filesystem::path &src, const std::filesystem::path &dst ) const {
	if ( !is_logged_in() )
		throw sandbox::virtual_machine_exception( "There is not logged user" );

	auto job = VixVM_CopyFileFromGuestToHost( vm_, src.string().c_str(), dst.string().c_str(), 0, VIX_INVALID_HANDLE, nullptr, nullptr );
	auto error = VixJob_Wait( job, VIX_PROPERTY_NONE );
	Vix_ReleaseHandle( job );
	if ( VIX_FAILED( error ) )
		throw sandbox::virtual_machine_exception( Vix_GetErrorText( error, nullptr ) );
}

void sandbox::vmware_virtual_machine::run_cmd( std::string_view command ) const {
	if ( !is_logged_in() )
		throw sandbox::virtual_machine_exception( "There is not logged user" );

	auto job = VixVM_RunScriptInGuest( vm_, nullptr, command.data(), 0, VIX_INVALID_HANDLE, nullptr, nullptr );
	auto error = VixJob_Wait( job, VIX_PROPERTY_NONE );
	Vix_ReleaseHandle( job );
	if ( VIX_FAILED( error ) )
		throw sandbox::virtual_machine_exception( Vix_GetErrorText( error, nullptr ) );
}

void sandbox::vmware_virtual_machine::run_exe( const std::filesystem::path &exe_path, std::string_view params ) const {
	if ( !is_logged_in() )
		throw sandbox::virtual_machine_exception( "There is not logged user" );

	auto job = VixVM_RunProgramInGuest( vm_, exe_path.string().c_str(), params.data(), 0, VIX_INVALID_HANDLE, nullptr, nullptr );
	auto error = VixJob_Wait( job, VIX_PROPERTY_NONE );
	Vix_ReleaseHandle( job );
	if ( VIX_FAILED( error ) )
		throw sandbox::virtual_machine_exception( Vix_GetErrorText( error, nullptr ) );
}

VixHandle sandbox::vmware_virtual_machine::create_snapshot(std::string_view name, bool include_memory) const {
	if (!is_booted())
		throw virtual_machine_exception("Machine is not booted");

	VixHandle snapshot_handle = VIX_INVALID_HANDLE;

	const auto flags = include_memory ? VIX_SNAPSHOT_INCLUDE_MEMORY : 0;

	auto job = VixVM_CreateSnapshot(vm_, name.data(), nullptr, flags, VIX_INVALID_HANDLE, nullptr, nullptr);
	auto error = VixJob_Wait(job, VIX_PROPERTY_JOB_RESULT_HANDLE, &snapshot_handle, VIX_PROPERTY_NONE);
	Vix_ReleaseHandle(job);
	if (VIX_FAILED(error))
		throw virtual_machine_exception(Vix_GetErrorText(error, nullptr));
	if (snapshot_handle == VIX_INVALID_HANDLE)
		throw virtual_machine_exception("Snapshot wasn't saved");

	return snapshot_handle;
}

void sandbox::vmware_virtual_machine::remove_snapshot(VixHandle& snapshot) const {
	if (snapshot == VIX_INVALID_HANDLE)
		return;

	if (!is_booted())
		throw virtual_machine_exception("Machine is not booted");

	auto job = VixVM_RemoveSnapshot(vm_, snapshot, 0, nullptr, nullptr);
	VixJob_Wait(job, VIX_PROPERTY_NONE);
	Vix_ReleaseHandle(job);
}

void sandbox::vmware_virtual_machine::revert_to_snapshot(VixHandle snapshot) const 
{
	if (snapshot == VIX_INVALID_HANDLE)
		return;

	if (!is_booted())
		throw virtual_machine_exception("Machine is not booted");

	auto job = VixVM_RevertToSnapshot(vm_, snapshot, 0, VIX_INVALID_HANDLE, nullptr, nullptr);
	auto error = VixJob_Wait(job, VIX_PROPERTY_NONE);
	Vix_ReleaseHandle(job);
	if (VIX_FAILED(error))
		throw virtual_machine_exception(Vix_GetErrorText(error, nullptr));
}