import os
import sys

# get the current dir
PROJECT_PATH = os.getcwd()
DIR_PATH = '\\'.join(PROJECT_PATH.split('\\')[0:-2]) + r'\Yara'


def get_log_yara_rules(file_path):
    """ A function that run the
    :return: the file path
    :rtype: str
    """
    yara_path = DIR_PATH + r"\yara64.exe "

    # run the malware detectors
    log = os.popen(yara_path + (DIR_PATH + r'\malwareDetector.yar ') + file_path).read()
    log += os.popen(yara_path + (DIR_PATH + r'\ourMalwareDetector.yar ') + file_path).read()
    return log


def main(file_path):
    result = get_log_yara_rules(file_path)
    if result == '':
        print("Not Found")
    else:
        results = ""
        #  order and print the result
        for element in result[:-2].split('\n'):
            results += element[0: element.find(' ')].upper() + " found in the file" + '\n'
        print(results)


if __name__ == "__main__":
    file = sys.argv[1]
    main(file)
