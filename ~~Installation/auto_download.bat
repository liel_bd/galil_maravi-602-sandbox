@echo off

cd /D "%~dp0"
if not "%1"=="am_admin" (powershell start -verb runas '%0' am_admin & exit/b)

@echo on

Rem install choco
powershell -command "Set-ExecutionPolicy Bypass -Scope Process"
powershell -command "Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))"

Rem install software
powershell -command "choco install python -y"
powershell -command "choco install mongodb --pre"
powershell -command "choco upgrade mongodb --pre"
powershell -command "choco install nodejs -y"
powershell -command "choco install vmwareworkstation -y"
powershell -command "choco upgrade vmwareworkstation"

Rem install python packages
pip install scapy
pip install sockets
pip install logging
pip install requests
pip install os-sys

Rem install nodemon
npm install -g nodemon

Rem install nodejs packages
cd ..\servers\backend
npm install

Rem install reactjs packages
cd ..\frontend
npm install


