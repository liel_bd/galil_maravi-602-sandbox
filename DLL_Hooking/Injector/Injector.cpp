// Injector.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <fstream>
#include <chrono>
#include <thread>
#include "HideVM.cpp"

void PrintError(LPTSTR lpszFunction);
BOOL WINAPI InjectDll(__in LPCWSTR lpcwszDll, __in LPCWSTR targetPath);

int main(int argc, char* argv[])
{

    clearVmValues(); // clear the values from the resistry

    wchar_t selfdir[MAX_PATH] = { 0 };
    wchar_t testMalware[MAX_PATH] = { 0 };
    std::string resultPath = "";
    GetModuleFileName(NULL, selfdir, MAX_PATH);
    PathRemoveFileSpec(selfdir);

    for (int i = 0; i < MAX_PATH; i++) {
        testMalware[i] = argv[1][i];
    }
    int resultPathLength = std::string(argv[0]).find("Injector.exe");
    for (int i = 0; i < resultPathLength; i++) {
        resultPath += argv[0][i];
    }

    resultPath += "result.txt";

    std::ofstream resultFile;
    resultFile.open("resultPath.txt");
    resultFile << resultPath.c_str();
    resultFile.close();

    std::wstring dllPath = std::wstring(selfdir) + TEXT("\\Hooking.dll");
    std::wstring targetPath = std::wstring(testMalware);

    if (InjectDll(dllPath.c_str(), targetPath.c_str())) {
        printf("Dll was successfully injected.\n");
    }
    else {
        printf("Terminating the Injector app...");
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    return 0;

}


BOOL WINAPI InjectDll(__in LPCWSTR lpcwszDll, __in LPCWSTR targetPath)
{
    SIZE_T nLength;
    LPVOID lpLoadLibraryW = NULL;
    LPVOID lpRemoteString;
    STARTUPINFO             startupInfo;
    PROCESS_INFORMATION     processInformation;

    memset(&startupInfo, 0, sizeof(startupInfo));
    startupInfo.cb = sizeof(STARTUPINFO);

    if (!CreateProcess(targetPath, NULL, NULL, NULL, FALSE,
        CREATE_SUSPENDED, NULL, NULL, &startupInfo, &processInformation))
    {
        //PrintError(TEXT("CreateProcess"));
        return FALSE;
    }

    lpLoadLibraryW = GetProcAddress(GetModuleHandle(L"KERNEL32.DLL"), "LoadLibraryW");

    if (!lpLoadLibraryW)
    {
        //PrintError(TEXT("GetProcAddress"));
        // close process handle
        CloseHandle( processInformation.hProcess);
        return FALSE;
    }

    nLength = wcslen(lpcwszDll) * sizeof(WCHAR);

    // allocate mem for dll name
    lpRemoteString = VirtualAllocEx(processInformation.hProcess, NULL, nLength + 1, MEM_COMMIT, PAGE_READWRITE);
    if (!lpRemoteString)
    {
        //PrintError(TEXT("VirtualAllocEx"));

        // close process handle
        CloseHandle(processInformation.hProcess);

        return FALSE;
    }

    // write dll name
    if (!WriteProcessMemory(processInformation.hProcess, lpRemoteString, lpcwszDll, nLength, NULL)) {

        //PrintError(TEXT("WriteProcessMemory"));
        // free allocated memory
        VirtualFreeEx(processInformation.hProcess, lpRemoteString, 0, MEM_RELEASE);

        // close process handle
        CloseHandle(processInformation.hProcess);

        return FALSE;
    }

    // call loadlibraryw
    HANDLE hThread = CreateRemoteThread(processInformation.hProcess, NULL, NULL, (LPTHREAD_START_ROUTINE)lpLoadLibraryW, lpRemoteString, NULL, NULL);

    if (!hThread) {
        //PrintError(TEXT("CreateRemoteThread"));
    }
    else {
        WaitForSingleObject(hThread, 4000);

        //resume suspended process
        ResumeThread(processInformation.hThread);
    }

    //  free allocated memory
    VirtualFreeEx(processInformation.hProcess, lpRemoteString, 0, MEM_RELEASE);

    // close process handle
    CloseHandle(processInformation.hProcess);

    return TRUE;
}

void PrintError(LPTSTR lpszFunction)
{
    // Retrieve the system error message for the last-error code

    LPVOID lpMsgBuf;
    LPVOID lpDisplayBuf;
    DWORD dw = GetLastError();

    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER |
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        dw,
        MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
        (LPTSTR)&lpMsgBuf,
        0, NULL);

    // Display the error message

    lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
        (lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR));
    StringCchPrintf((LPTSTR)lpDisplayBuf,
        LocalSize(lpDisplayBuf) / sizeof(TCHAR),
        TEXT("%s failed with error %d: %s"),
        lpszFunction, dw, lpMsgBuf);

    wprintf(L"%s", lpDisplayBuf);

    LocalFree(lpMsgBuf);
    LocalFree(lpDisplayBuf);
}