
#include <Windows.h>
#include <string>

using std::wstring;

/// <summary>
///		A function that clean registry values
/// </summary>
/// 
/// <param name="key">
///		the name of the variable in the registry files
/// </param>
/// 
/// <param name="value">
///		the value of the specific variable
/// </param>
void clearValue(LPCWSTR key, LPCWSTR value) {

	HKEY hkey = 0;
	BYTE* arr = 0;
	int res = RegOpenKeyEx(HKEY_LOCAL_MACHINE, key, 0, KEY_WRITE, &hkey);

	RegSetValueEx(hkey, value, 0, REG_SZ, arr, 0);

	RegCloseKey(hkey);

}

/// <summary>
///		A function that clean suspicious values from the registry
/// </summary>
void clearVmValues() {

	clearValue(TEXT("HARDWARE\\DESCRIPTION\\System\\BIOS"), TEXT("SystemManufacturer"));
	clearValue(TEXT("HARDWARE\\DESCRIPTION\\System\\BIOS"), TEXT("SystemProductName"));
	clearValue(TEXT("HARDWARE\\DEVICEMAP\\Scsi\\Scsi Port 0\\Scsi Bus 0\\Target Id 0\\Logical Unit Id 0"), TEXT("Identifier"));
	clearValue(TEXT("HARDWARE\\DEVICEMAP\\Scsi\\Scsi Port 1\\Scsi Bus 0\\Target Id 0\\Logical Unit Id 0"), TEXT("Identifier"));
	clearValue(TEXT("HARDWARE\\DEVICEMAP\\Scsi\\Scsi Port 2\\Scsi Bus 0\\Target Id 0\\Logical Unit Id 0"), TEXT("Identifier"));
	clearValue(TEXT("SYSTEM\\CurrentControlSet\\Services\\Disk\\Enum"), TEXT("0"));
}
