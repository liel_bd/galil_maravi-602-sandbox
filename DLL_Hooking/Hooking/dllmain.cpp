
#include "Hooks.h"


/// <summary>
///     A function the replace the address of the api function (syscall) with our function
/// </summary>
/// 
/// <param name="ppSystemFunction">
///     the address of the syscall function that the file access
/// </param>
/// 
/// <param name="pHookFunction">
///     the address of out function
/// </param>
void attachApiFunction(PVOID* ppSystemFunction, PVOID pHookFunction) {

    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    DetourAttach(ppSystemFunction, pHookFunction);
    LONG lError = DetourTransactionCommit();
    if (lError != NO_ERROR) {
        MessageBox(HWND_DESKTOP, L"Failed to detour", L"timb3r", MB_OK);
    }

}

/// <summary>
///     A function the replace the address of our function with the api function (syscall) 
/// </summary>
/// 
/// <param name="ppSystemFunction">
///     the address of the syscall function that the file access
/// </param>
/// 
/// <param name="pHookFunction">
///     the address of out function
/// </param>
bool detachApiFunction(PVOID* ppSystemFunction, PVOID pHookFunction) {

    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    DetourDetach(ppSystemFunction, pHookFunction);
    LONG lError = DetourTransactionCommit();
    if (lError != NO_ERROR) {
        MessageBox(HWND_DESKTOP, L"Failed to detour", L"timb3r", MB_OK);
        return FALSE;
    }
    return TRUE;

}

BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID p)
{

    ifstream in;
    in.open("resultPath.txt");
    in >> resultPath;
    in.close();

    switch (dwReason) {
        case DLL_PROCESS_ATTACH:
        {

            attachApiFunction((PVOID*)&True_ReadFile, HookedReadFile);
            attachApiFunction((PVOID*)&True_LoadLibrary, HookedLoadLibrary);
            attachApiFunction((PVOID*)&True_GetUserName, HookedGetUserName);
            attachApiFunction((PVOID*)&True_RegCreateKey, HookedRegCreateKey);
            attachApiFunction((PVOID*)&True_SetWindowsHookEx, HookedSetWindowsHookEx);
            attachApiFunction((PVOID*)&True_FindWindow, HookedFindWindow);
            attachApiFunction((PVOID*)&True_EnumServicesStatus, HookedEnumServicesStatus);
            attachApiFunction((PVOID*)&True_GetDiskFreeSpace, HookedGetDiskFreeSpace);
            attachApiFunction((PVOID*)&True_FindFirstFile, HookedFindFirstFile);
            attachApiFunction((PVOID*)&True_FindNextFile, HookedFindNextFile);
            attachApiFunction((PVOID*)&True_GetSystemInfo, HookedGetSystemInfo);
            attachApiFunction((PVOID*)&True_MapViewOfFile, HookedMapViewOfFile);
            attachApiFunction((PVOID*)&True_CreateProcess, HookedCreateProcess);
            attachApiFunction((PVOID*)&True_QueueUserAPC, HookedQueueUserAPC);

        }
        break;
     
        case DLL_PROCESS_DETACH:
        {

            if (!(detachApiFunction((PVOID*)&True_ReadFile, HookedReadFile)
               && detachApiFunction((PVOID*)&True_LoadLibrary, HookedLoadLibrary)
               && detachApiFunction((PVOID*)&True_GetUserName, HookedGetUserName)
               && detachApiFunction((PVOID*)&True_RegCreateKey, HookedRegCreateKey)
               && detachApiFunction((PVOID*)&True_SetWindowsHookEx, HookedSetWindowsHookEx)
               && detachApiFunction((PVOID*)&True_FindWindow, HookedFindWindow)
               && detachApiFunction((PVOID*)&True_EnumServicesStatus, HookedEnumServicesStatus)
               && detachApiFunction((PVOID*)&True_GetDiskFreeSpace, HookedGetDiskFreeSpace)
               && detachApiFunction((PVOID*)&True_FindFirstFile, HookedFindFirstFile)
               && detachApiFunction((PVOID*)&True_FindNextFile, HookedFindNextFile)
               && detachApiFunction((PVOID*)&True_GetSystemInfo, HookedGetSystemInfo)
               && detachApiFunction((PVOID*)&True_MapViewOfFile, HookedMapViewOfFile)
               && detachApiFunction((PVOID*)&True_CreateProcess, HookedCreateProcess)
               && detachApiFunction((PVOID*)&True_QueueUserAPC, HookedQueueUserAPC))) {
                return FALSE;
            }

        }
        break;
    }
    return TRUE;

}
