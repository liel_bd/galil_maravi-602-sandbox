#pragma once

#include "pch.h"
#include <stdio.h>
#include <windows.h>
#include <winsock2.h>
#include <iostream>
#include "detours/src/detours.h"
#include <fstream>
using namespace std;

// Address of the real APIs.
BOOL(WINAPI* True_ReadFile)(HANDLE, LPVOID, DWORD, LPDWORD, LPOVERLAPPED) = ReadFile;
HMODULE(WINAPI* True_LoadLibrary)(LPCWSTR) = LoadLibrary;
BOOL(WINAPI* True_GetUserName)(LPWSTR, LPDWORD) = GetUserName;
HANDLE(WINAPI* True_CreateRemoteThread)(HANDLE, LPSECURITY_ATTRIBUTES, SIZE_T, LPTHREAD_START_ROUTINE, LPVOID, DWORD, LPDWORD) = CreateRemoteThread;
LSTATUS(WINAPI* True_RegCreateKey)(HKEY, LPCWSTR, PHKEY) = RegCreateKey;
HHOOK(WINAPI* True_SetWindowsHookEx)(int, HOOKPROC, HINSTANCE, DWORD) = SetWindowsHookEx;
HWND(WINAPI* True_FindWindow)(LPCWSTR, LPCWSTR) = FindWindow;
BOOL(WINAPI* True_EnumServicesStatus)(SC_HANDLE, DWORD, DWORD, LPENUM_SERVICE_STATUSW, DWORD, LPDWORD, LPDWORD, LPDWORD) = EnumServicesStatus;
BOOL(WINAPI* True_GetDiskFreeSpace)(LPCWSTR, LPDWORD, LPDWORD, LPDWORD, LPDWORD) = GetDiskFreeSpace;
HANDLE(WINAPI* True_FindFirstFile)(LPCWSTR, LPWIN32_FIND_DATAW) = FindFirstFile;
BOOL(WINAPI* True_FindNextFile)(HANDLE, LPWIN32_FIND_DATAW) = FindNextFile;
void(WINAPI* True_GetSystemInfo)(LPSYSTEM_INFO) = GetSystemInfo;
LPVOID(WINAPI* True_MapViewOfFile)(HANDLE, DWORD, DWORD, DWORD, SIZE_T) = MapViewOfFile;
BOOL(WINAPI* True_CreateProcess)(LPCWSTR, LPWSTR, LPSECURITY_ATTRIBUTES, LPSECURITY_ATTRIBUTES, BOOL, DWORD, LPVOID, LPCWSTR, LPSTARTUPINFOW, LPPROCESS_INFORMATION) = CreateProcess;
DWORD(WINAPI* True_QueueUserAPC)(PAPCFUNC, HANDLE, ULONG_PTR) = QueueUserAPC;

BOOL WINAPI HookedReadFile(HANDLE hFile, LPVOID lpBuffer, DWORD nNumberOfBytesToRead, LPDWORD lpNumberOfBytesRead, LPOVERLAPPED lpOverlapped);
HMODULE WINAPI HookedLoadLibrary(LPCWSTR lpLibFileName);
BOOL WINAPI HookedGetUserName(LPWSTR lpBuffer, LPDWORD pcbBuffer);
HANDLE WINAPI HookedCreateRemoteThread(HANDLE hProcess, LPSECURITY_ATTRIBUTES lpThreadAttributes, SIZE_T dwStackSize, LPTHREAD_START_ROUTINE lpStartAddress, LPVOID lpParameter, DWORD dwCreationFlags, LPDWORD lpThreadId);
LSTATUS WINAPI HookedRegCreateKey(HKEY hKey, LPCWSTR lpSubKey, PHKEY phkResult);
HHOOK WINAPI HookedSetWindowsHookEx(int idHook, HOOKPROC lpfn, HINSTANCE hmod, DWORD dwThreadId);
HWND WINAPI HookedFindWindow(LPCWSTR lpClassName, LPCWSTR lpWindowName);
BOOL WINAPI HookedEnumServicesStatus(SC_HANDLE hSCManager, DWORD dwServiceType, DWORD dwServiceState, LPENUM_SERVICE_STATUSW lpServices, DWORD cbBufSize, LPDWORD pcbBytesNeeded, LPDWORD lpServicesReturned, LPDWORD lpResumeHandle);
BOOL WINAPI HookedGetDiskFreeSpace(LPCSTR lpRootPathName, LPDWORD lpSectorsPerCluster, LPDWORD lpBytesPerSector, LPDWORD lpNumberOfFreeClusters, LPDWORD lpTotalNumberOfClusters);
HANDLE WINAPI HookedFindFirstFile(LPCWSTR lpFileName, LPWIN32_FIND_DATAW lpFindFileData);
BOOL WINAPI HookedFindNextFile(HANDLE hFindFile, LPWIN32_FIND_DATAW lpFindFileData);
void WINAPI HookedGetSystemInfo(LPSYSTEM_INFO lpSystemInfo);
LPVOID WINAPI HookedMapViewOfFile(HANDLE hFileMappingObject, DWORD  dwDesiredAccess, DWORD  dwFileOffsetHigh, DWORD  dwFileOffsetLow, SIZE_T dwNumberOfBytesToMap);
BOOL WINAPI HookedCreateProcess(LPCWSTR lpApplicationName, LPWSTR lpCommandLine, LPSECURITY_ATTRIBUTES lpProcessAttributes, LPSECURITY_ATTRIBUTES lpThreadAttributes, BOOL bInheritHandles, DWORD dwCreationFlags, LPVOID lpEnvironment, LPCWSTR lpCurrentDirectory, LPSTARTUPINFOW lpStartupInfo, LPPROCESS_INFORMATION lpProcessInformation);
DWORD WINAPI HookedQueueUserAPC(PAPCFUNC pfnAPC, HANDLE hThread, ULONG_PTR dwData);

string wideToChar(LPCWSTR wide);

string resultPath = "";


//the hook functions - catches and report the syscall


BOOL WINAPI HookedReadFile(HANDLE hFile, LPVOID lpBuffer, DWORD nNumberOfBytesToRead, LPDWORD lpNumberOfBytesRead, LPOVERLAPPED lpOverlapped) {

    ofstream out;
    out.open(resultPath, ios_base::app);
    out << "FILE_READ" << endl;
    out.close();

    return True_ReadFile(hFile, lpBuffer, nNumberOfBytesToRead, lpNumberOfBytesRead, lpOverlapped);

}

HMODULE WINAPI HookedLoadLibrary(LPCWSTR lpLibFileName) {

    ofstream out;
    out.open(resultPath, ios_base::app);
    out << "LOAD_DLL" << endl;
    out.close();

    return True_LoadLibrary(lpLibFileName);

}

BOOL WINAPI HookedGetUserName(LPWSTR lpBuffer, LPDWORD pcbBuffer) {

    ofstream out;
    out.open(resultPath, ios_base::app);
    out << "GET_USER_NAME" << endl;
    out.close();

    return True_GetUserName(lpBuffer, pcbBuffer);

}

HANDLE WINAPI HookedCreateRemoteThread(HANDLE hProcess, LPSECURITY_ATTRIBUTES lpThreadAttributes, SIZE_T dwStackSize, LPTHREAD_START_ROUTINE lpStartAddress, LPVOID lpParameter, DWORD dwCreationFlags, LPDWORD lpThreadId) {

    ofstream out;
    out.open(resultPath, ios_base::app);
    out << "CREATE_REMOTE_THREAD" << endl;
    out.close();

    return True_CreateRemoteThread(hProcess, lpThreadAttributes, dwStackSize, lpStartAddress, lpParameter, dwCreationFlags, lpThreadId);

}

inline LSTATUS __stdcall HookedRegCreateKey(HKEY hKey, LPCWSTR lpSubKey, PHKEY phkResult) {

    ofstream out;
    out.open(resultPath, ios_base::app);
    out << "REG_CREATE_KEY" << endl;
    out.close();

    return True_RegCreateKey(hKey, lpSubKey, phkResult);

}

inline HHOOK __stdcall HookedSetWindowsHookEx(int idHook, HOOKPROC lpfn, HINSTANCE hmod, DWORD dwThreadId) {
    
    ofstream out;
    out.open(resultPath, ios_base::app);
    out << "SET_WINDOWS_HOOK_EX" << endl;
    out.close();
    
    return True_SetWindowsHookEx(idHook, lpfn, hmod, dwThreadId);

}

inline HWND __stdcall HookedFindWindow(LPCWSTR lpClassName, LPCWSTR lpWindowName) {

    ofstream out;
    out.open(resultPath, ios_base::app);
    out << "FIND_WINDOW" << endl;
    out.close();

    return True_FindWindow(lpClassName, lpWindowName);

}

inline BOOL __stdcall HookedEnumServicesStatus(SC_HANDLE hSCManager, DWORD dwServiceType, DWORD dwServiceState, LPENUM_SERVICE_STATUSW lpServices, DWORD cbBufSize, LPDWORD pcbBytesNeeded, LPDWORD lpServicesReturned, LPDWORD lpResumeHandle) {
    
    ofstream out;
    out.open(resultPath, ios_base::app);
    out << "ENUM_SERVICES_STATUS" << endl;
    out.close();
    
    return True_EnumServicesStatus(hSCManager, dwServiceType, dwServiceState, lpServices, cbBufSize, pcbBytesNeeded, lpServicesReturned, lpResumeHandle);

}

inline BOOL __stdcall HookedGetDiskFreeSpace(LPCSTR lpRootPathName, LPDWORD lpSectorsPerCluster, LPDWORD lpBytesPerSector, LPDWORD lpNumberOfFreeClusters, LPDWORD lpTotalNumberOfClusters) {
    
    // Returns fake values of large drive to prevent detection of VM environment.
    *lpSectorsPerCluster = 8;
    *lpBytesPerSector = 512;
    *lpNumberOfFreeClusters = 30000000;
    *lpTotalNumberOfClusters = 60000000;
    
    ofstream out;
    out.open(resultPath, ios_base::app);
    out << "GET_DISK_FREE_SPACE" << endl;
    out.close();

    return TRUE;

}

inline HANDLE __stdcall HookedFindFirstFile(LPCWSTR lpFileName, LPWIN32_FIND_DATAW lpFindFileData) {

    HANDLE handle = True_FindFirstFile(lpFileName, lpFindFileData);
    string name = wideToChar(lpFindFileData->cFileName);

    if (name != ""
        && name.find("vm") != string::npos
        && name.find(".sys") != string::npos) {
        lpFindFileData->cFileName[0] = '_';
        handle = INVALID_HANDLE_VALUE;
    }

    return handle;

}

inline BOOL __stdcall HookedFindNextFile(HANDLE hFindFile, LPWIN32_FIND_DATAW lpFindFileData) {
    
    True_FindNextFile(hFindFile, lpFindFileData);
    string name = wideToChar(lpFindFileData->cFileName);

    if (name != ""
     && name.find("vm") != string::npos
     && name.find(".sys") != string::npos) {
        lpFindFileData->cFileName[0] = '_';
    }

    return TRUE;

}

inline void __stdcall HookedGetSystemInfo(LPSYSTEM_INFO lpSystemInfo) {

    True_GetSystemInfo(lpSystemInfo);
    lpSystemInfo->dwNumberOfProcessors = 8;

    ofstream out;
    out.open(resultPath, ios_base::app);
    out << "GET_SYSTEM_INFO" << endl;
    out.close();

}

inline LPVOID __stdcall HookedMapViewOfFile(HANDLE hFileMappingObject, DWORD dwDesiredAccess, DWORD dwFileOffsetHigh, DWORD dwFileOffsetLow, SIZE_T dwNumberOfBytesToMap) {
    
    ofstream out;
    out.open(resultPath, ios_base::app);
    out << "MAP_VIEW_OF_FILE" << endl;
    out.close();
    
    return True_MapViewOfFile(hFileMappingObject, dwDesiredAccess, dwFileOffsetHigh, dwFileOffsetLow, dwNumberOfBytesToMap);

}

inline BOOL __stdcall HookedCreateProcess(LPCWSTR lpApplicationName, LPWSTR lpCommandLine, LPSECURITY_ATTRIBUTES lpProcessAttributes, LPSECURITY_ATTRIBUTES lpThreadAttributes, BOOL bInheritHandles, DWORD dwCreationFlags, LPVOID lpEnvironment, LPCWSTR lpCurrentDirectory, LPSTARTUPINFOW lpStartupInfo, LPPROCESS_INFORMATION lpProcessInformation) {
    
    ofstream out;
    out.open(resultPath, ios_base::app);
    out << "CREATE_PROCESS" << endl;
    out.close();
    
    return True_CreateProcess(lpApplicationName, lpCommandLine, lpProcessAttributes, lpThreadAttributes, bInheritHandles, dwCreationFlags, lpEnvironment, lpCurrentDirectory, lpStartupInfo, lpProcessInformation);

}

inline DWORD __stdcall HookedQueueUserAPC(PAPCFUNC pfnAPC, HANDLE hThread, ULONG_PTR dwData) {
    
    ofstream out;
    out.open(resultPath, ios_base::app);
    out << "QUEUE_USER_APC" << endl;
    out.close();
    
    return True_QueueUserAPC(pfnAPC, hThread, dwData);

}

// Converts widechar to string.
string wideToChar(LPCWSTR wide) {

    int len = WideCharToMultiByte(CP_UTF8, 0, wide, -1, NULL, 0, 0, 0);
    if (len > 0) {

        char* chars = new char[len + 1];
        if (chars) {
            int resLen = WideCharToMultiByte(CP_UTF8, 0, wide, -1, &chars[0], len, 0, 0);

            if (resLen == len) {
                string result = string(chars);
                delete[] chars;
                return string(result);
            }
        }

    }

    return "";

}
